![npm](https://img.shields.io/npm/v/scanargs)
![npm](https://img.shields.io/npm/dt/scanargs)  
[![pipeline status](https://gitlab.com/entwicklerFR/scanargs/badges/master/pipeline.svg?style=flat)](https://gitlab.com/entwicklerFR/scanargs/commits/master)
[![coverage report](https://gitlab.com/entwicklerFR/scanargs/badges/master/coverage.svg)](https://gitlab.com/entwicklerFR/scanargs/commits/master)

# scanargs
Typescript Node.js package to parse command-line arguments.
- No dependencies
- Simple to use
- Can manage environment and configuration file variables too
- For Typescript and Javascript

## Technical notes
- Written in Typescript and compiled in ECMAScript with target version ES2015 (aka ES6)
- Node.js >= 8.0.0

## Installation
```
$ npm install --save scanargs
```

## Command-line arguments
In your command, you can set arguments as follows:  
```
$ example --port=8080 --logger=true
```
is equivalent to:
```

$ example --port 8080 --logger true
$ example -p=8080 -l=true
$ example -p 8080 -l true
```
with package.json scripts, add an extra double hyphen:
```
$ npm run script -- --port=8080 --logger=true
```

## Usage
### Typescript example
#### import
In your Typescript code, `import` the scanargs package as follows:
```
import { Scanargs } from ('scanargs');
```

#### Code
```js
import { Scanargs } from 'scanargs';

const scan: Scanargs = new Scanargs(process.argv, [
  { name: 'port', default: 3000 },
  { name: 'logger', default: false },
  { name: 'throwError' },
]);

console.log(`${JSON.stringify(scan.scan(), null, 2)}`);
```

#### Command
```
$ ts-node example.ts --port="8080" -l=true --throwError=false --arg=qwerty
```

#### Console output
```json
{
  "port": 8080,
  "logger": true,
  "throwError": "false"
}
```
> Note: As 'arg' is not defined in options, it is not returned/recognized as an argument.

### Javascript example
#### require
In your javascript code, `require` the scanargs package as follows:
```
const { Scanargs } = require('scanargs');
```
#### Code
```js
const { Scanargs } = require('scanargs');
const scan = new Scanargs(process.argv, [{name: 'param'}]);

console.log(`bin [ ${scan.getNode()} ] file [ ${scan.getNode()} ] param [ ${scan.scan().param} ]`);
```

#### Command
```
$ node example.js --param=qwerty
```

#### Console output
```
bin [ /usr/bin/node ] file [ /usr/bin/node ] param [ qwerty ]
```

## Class
### Constructor
It creates a Scanargs object which will parse the command arguments.  

It needs only one mandatory parameter:
- it's classically the result of the process.argv node property  

The second one is optional:  
- it's a definition of the arguments you want to retrieve and use in your code  

#### Typescript example
```js
// your options: arguments you expect to be passed in command-line
const myOptions: Option[] = [
    { name: 'myFirstArgument' },
    { name: 'mySecondArgument' }
];

// your parser
let scan: Scanargs;

// you can instantiate directly as follows
scan = new Scanargs(process.argv, myOptions);

// or as below
scan = new Scanargs(process.argv);
// or
scan = new Scanargs(process.argv, []);
// or 
scan = new Scanargs(process.argv, [{{ name: 'foo', default: 'oof' }}]);
// and use the setter
scan.options = myOptions;
```

### Options
It's an array of `Option` passed to the constructor or to the `options` setter.  
An Option is defined as follows:
```js
{
  name: string;
  alias: string;
  description: string;
  default: any;
}
```
- **name** is mandatory.
  + It's the name (case sensitive) of a command argument you want to retrieve.
  + It's the string you type after the double hyphen (--).  
  (for example: 'foo' => '--foo')
- **alias** is optional.
  + It's the argument short-name (case sensitive).
  + It's the string you type after the hyphen (-).
  + If it's not set the default alias is defined to the first letter of name.  
  (for example: if name is 'foo' then the default alias is 'f' => '-f')
- **description** is optional.
  + If it's defined, it is displayed by the default help.
- **default** is optional.
  + If it's defined, it gives its type (`number`, `string`, `boolean`)  to the passed value.
  + If it's not defined, the value type is always `string`

### Options setting
As shown in the above examples, you can set Options with the `options` setter in the same way you do with the constructor.  

#### Typescript example
```js
const scan = new Scanargs(process.argv);
scan.options = [
    { name: 'myFirstArgument' },
    { name: 'mySecondArgument' }
];
```

### scan()
It returns the parsed arguments into a json object.  
So to retrieve the argument value you want, just get the key of this one:  

#### Example
```js
console.log(scan.scan().myFirstArgument);
```

#### Behavior
1. Try to get the command options values
1. Then, if not defined, try to get the environment variables values
   * values returned by the Node.js `process.env` property 
1. Then, if not defined, try to get the configuration file variables values
   * values set in a `.config.json` file defined into the current working directory of the Node.js process 
   * File content example: {"port": 3000, "logger": true}
1. Then, if not defined, try to get the option default value


### Help
#### Default behavior
`-h` and `--help` options are interpreted by default.  
When you call your program with one of this both arguments, `scan()` displays a default help based on the Options description properties if defined.  
By default, `scan()` displays the help and exits the program.  
You can use the `helpExit` setter to deactivate the process exit after the help display.  
You can use the `showHelp` setter to deactivate the help display.  

For example, if options are defined as follows  
```js
scan.options = [
    { name: 'port', description: 'port number' },
    { name: 'logger' }
];
```
the help will be displayed as below 
```
Usage :
-------

program [options]


Options :
---------

-h, --help: display this help
-p, --port: port number
-l, --logger: <no description available>
```
#### Customized behavior
To override this default behavior, you have to set `showHelp` to false and define your own `help` option.  
Then in your code just retrieve the `help` argument and its value and manage the help yourself exactly as you want.

## Other examples
*For more details, you can see examples in the [GitLab repository](https://gitlab.com/entwicklerFR/scanargs/tree/master/examples).*  
  
Let's assume that you define these Options in a Typescript file: `example.ts`.  
> Note: In Javascript, execute an `example.js` file with `node` instead of `ts-node`.
```js
[
  { name: 'port', default: 3000 },
  { name: 'logger', default: false },
  { name: 'throwError' }
]
```
Then:  
### Example 1
```
$ ts-node example.ts -p=8080 --logger true --throwError=false
```
returns  
```json
{
  "port": 8080,
  "logger": true,
  "throwError": "false"
}
```
> *throwError* is of type string and not of boolean type since it has no boolean default value  

### Example 2
```
$ ts-node example.ts -p 8080
```
returns  
```json
{
  "port": 8080,
  "logger": false
}
```
> *logger* has a default boolean value set to false, so this value is returned  
> *throwError* has no default value so nothing is returned  

### Example 3
```
$ ts-node example.ts -p -l --foo
```
returns  
```json
{
  "port": 3000,
  "logger": true
}
```
> the *port* argument is present without any value but the returned value is set to its default value  
> the *logger* argument is present without any value but the returned value is set to true because the type of its default value is boolean  
> the *foo* argument is ignored because it is not an Option  

### Example 4
```
$ ts-node example.ts --throwError
```
returns  
```json
{
  "port": 3000,
  "logger": false,
  "throwError": ""
}
```
> the *port* argument is not present but the returned value is set to its default value  
> the *logger* argument is not present but the returned value is set to its default value  
> the *throwError* argument is present without any value and the option default value is not set, so empty string is returned  

### Example 5
```
$ export port=6000; ts-node example.ts --logger true
```
returns  
```json
{
  "port": 6000,
  "logger": true
}
```
> the *port* argument is not present but the returned value is set to its environment value  