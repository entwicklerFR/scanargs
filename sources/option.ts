/**
 * An option is a definition for an expected command parameter.
 * @interface Option
 * @property name - parameter name
 * @property [alias] - parameter short-name
 * @property [description] - comment on this parameter
 * @property [default] - default value if no value passed to this parameter
 */
interface Option {
  name: string;
  alias?: string;
  description?: string;
  default?: any;
}

/**
 * An OptionValue is composed of an option definition name and its value.
 * This value is retrieved by scanning the command arguments.
 * @interface OptionValue
 * @property name - parameter name
 * @property value - value of the parameter of name name
 */
interface OptionValue {
  name: string;
  value: any;
}

/**
 * Represents the list of the command line parameters with their values.
 * The key is the parameter name and the value its value.
 * @interface OptionsValues
 */
interface OptionsValues {
  [key: string]: any;
}

export { Option, OptionValue, OptionsValues };
