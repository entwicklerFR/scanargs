import * as path from 'path';
import { Option, OptionsValues, OptionValue } from './option';

/**
 * Scan an array and try to retrieve values according to an option definition.
 * @param argv - should be the value of the process.argv node property, in other words the command line arguments
 * @param [options] - definitions of expected command line arguments
 */
class Scanargs {
  private _argv: string[];
  private _options: Option[] = [];
  private _showHelp: boolean = true;
  private _helpExit: boolean = true;
  private _jsonConfig: any;

  constructor(argv: string[], options?: Option[]) {
    this._argv = [...argv];

    if (options !== undefined) {
      this._options = this._setOptions(options);
    }

    try {
      this._jsonConfig = require(path.resolve(process.cwd(), '.config.json'));
    } catch (error) {
      // do nothing
    }
  }

  /**
   * Returns arguments values in an object.
   * 1/ Try to get command options values
   * 2/ Then, if not defined, try to get the environment variables values
   * 3/ Then, if not defined, try to get the configuration file variables values
   * 4/ Then, if not defined, try to get the option default value
   */
  public scan(): any {
    let optionsValues: OptionsValues;
    const newOptionsValues: OptionValue[] = [];

    // is default help wanted ?
    if (this._showHelp) {
      const helpWanted: string[] = this._argv.filter(argument => argument === '--help' || argument === '-h');
      if (helpWanted.length > 0) {
        this._displayHelp();
      }
    }

    if (this._argv.length > 1) {
      for (const option of this._options) {
        let name: string | undefined;
        let value: any;

        // (1) we try to find a command line parameter of name: option.name
        for (let i = 2; i < this._argv.length; i++) {
          // if this parameter exists: we look for its value after the equal sign
          // if there is a value we assign this one
          // if not we try to assign the default value
          //
          // (a) tests the presence of "--name=*" or "-n=*"
          if (this._argv[i].startsWith(`-${option.alias}=`) || this._argv[i].startsWith(`--${option.name}=`)) {
            name = option.name;
            value = this._getTypedValue(this._captureAfterEqual(this._argv[i]), option.default);
            newOptionsValues.push({ name, value });
          }
          //
          // (b) tests the specific presence of "--name" or "-n"
          else if (this._argv[i] === `-${option.alias}` || this._argv[i] === `--${option.name}`) {
            name = option.name;
            // returns the next argument as value if this one exists and doesn't contain an hypen or a double-hyphen
            if (this._argv[i + 1] && !this._argv[i + 1].startsWith('-')) {
              value = this._getTypedValue(this._argv[i + 1], option.default);
              newOptionsValues.push({ name, value });
            }
            //
            // if the value doesn't exist
            else {
              // forces true only if its default value is of boolean type
              if (typeof this._getTypedValue(option.default, option.default) === 'boolean') {
                value = true;
              }

              if (value === undefined) {
                // (2) we try to assign its environment value
                value = this._getEnvValue(name, option.default);
              }

              if (value === undefined) {
                // (3) we try to assign its config file value
                value = this._getConfigValue(name, option.default);
              }

              if (value === undefined) {
                // (4) we try to assign its default value
                value = this._getTypedValue(option.default, option.default);
              }

              newOptionsValues.push({ name, value });
            }
          }
        }

        // if this current parameter doesn't exist in the command arguments list
        if (!name) {
          name = option.name;

          // (2) we try to assign its environment value
          value = this._getEnvValue(name, option.default);

          if (value === undefined) {
            // (3) we try to assign its config file value
            value = this._getConfigValue(name, option.default);
          }

          if (value === undefined) {
            // (4) we try to assign its default value
            value = this._getTypedValue(option.default, option.default);
          }

          newOptionsValues.push({ name, value });
        }
      }
    }

    // transform array to an OptionsValues object
    const mapped: any = newOptionsValues.map(item => ({ [item.name]: item.value }));
    optionsValues = Object.assign({}, ...mapped);
    return optionsValues;
  }

  public getExecPath(): string {
    return this._argv[0];
  }

  public getFilePath(): string {
    return this._argv[1];
  }

  public get options(): Option[] {
    return [...this._options];
  }

  public set options(options: Option[]) {
    this._options = this._setOptions(options);
  }

  public set showHelp(help: boolean) {
    this._showHelp = help;
  }

  public get showHelp(): boolean {
    return this._showHelp;
  }

  public set helpExit(exit: boolean) {
    this._helpExit = exit;
  }

  public get helpExit(): boolean {
    return this._helpExit;
  }

  /**
   * If it exists, returns the configuration file variable value casted with the type of the default value.
   * If not, returns undefined.
   * @param variableName
   * @param defaultValue
   */
  private _getConfigValue(variableName: string, defaultValue: any): any {
    let value: any; // undefined

    if (this._jsonConfig && this._jsonConfig[variableName] !== undefined && this._jsonConfig[variableName] !== null) {
      value = this._getTypedValue(this._jsonConfig[variableName], defaultValue);
    }

    return value;
  }

  /**
   * If it exists, returns the environment variable value casted with the type of the default value.
   * If not, returns undefined.
   * @param variableName
   * @param defaultValue
   */
  private _getEnvValue(variableName: string, defaultValue: any): any {
    let value: any; // undefined

    if (process.env[variableName] !== undefined && process.env[variableName] !== null) {
      value = this._getTypedValue(process.env[variableName], defaultValue);
    }

    return value;
  }

  /**
   * Returns the value casted with the type of the default value.
   * If no defaultValue we return the value which has a string type by default (or undefined).
   * @param value
   * @param defaultValue
   */
  private _getTypedValue(value: any, defaultValue: any): any {
    if (defaultValue === undefined || defaultValue === null) {
      // option and argument are defined/passed but there is no option default value nor argument value
      if (!value) {
        return '';
      } else {
        return value;
      }
    }

    if (!value) {
      return defaultValue;
    }

    if (typeof defaultValue === 'number') {
      return Number.parseInt(value, 10);
    }

    if (typeof defaultValue === 'boolean') {
      if (typeof value === 'boolean') {
        return value;
      } else {
        return value === 'true';
      }
    }

    return value;
  }

  private _setDefaultAlias(option: Option): Option {
    const newOption = { ...option };
    if (!newOption.alias) {
      newOption.alias = option.name.charAt(0);
    }
    return newOption;
  }

  private _setOptions(options: Option[]): Option[] {
    const newOptions: Option[] = [];
    for (const option of options) {
      newOptions.push(this._setDefaultAlias(option));
    }
    return newOptions;
  }

  /**
   * Retrieves the value after the equal sign
   * @example
   * // returns '3000'
   * _captureAfterEqual("--port=3000")
   * // returns ''
   * _captureAfterEqual("--port=")
   * // returns null
   * _captureAfterEqual("--port")
   */
  private _captureAfterEqual(source: string): string | null {
    const regex: any = /^[^=]+=\s*(.*)$/;
    const found: any = source.match(regex);
    if (found) {
      return found[1];
    } else {
      return found;
    }
  }

  // tslint:disable: no-console
  private _displayHelp(): void {
    console.log('');
    console.log('Usage :');
    console.log('-------');
    console.log('');
    console.log('program [options]');
    console.log('');
    console.log('');
    console.log('Options :');
    console.log('---------');
    console.log('');
    console.log('-h, --help: display this help');
    for (const option of this._options) {
      console.log(
        `-${option.alias}, --${option.name}: ${option.description ? option.description : '<no description available>'}`,
      );
    }
    console.log('');
    console.log('');
    if (this._helpExit) {
      process.exit(0);
    }
  }
  // tslint:enable: no-console
}

export { Scanargs };
