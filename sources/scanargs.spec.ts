import {} from 'jasmine';
import { Option } from './option';
import { Scanargs } from './scanargs';

describe('Scanargs', () => {
  const savedEnv = process.env;

  describe('Class construction', () => {
    it('An instantiation with the mandatory parameter should create a Scanargs object', () => {
      const scanargs: Scanargs = new Scanargs(new Array());
      expect(scanargs).toBeDefined();
    });

    it('An instantiation with both allowed parameters should create a Scanargs object', () => {
      const scanargs: Scanargs = new Scanargs(new Array(), new Array());
      expect(scanargs).toBeDefined();
    });
  });

  describe('Default help', () => {
    it('Set showHelp should work', () => {
      const scanargs: Scanargs = new Scanargs(new Array());
      const showHelp = scanargs.showHelp;
      scanargs.showHelp = !showHelp;
      expect(scanargs.showHelp).toBe(!showHelp);
    });

    it('Set helpExit should work', () => {
      const scanargs: Scanargs = new Scanargs(new Array());
      const helpExit = scanargs.helpExit;
      scanargs.helpExit = !helpExit;
      expect(scanargs.helpExit).toBe(!helpExit);
    });

    it('Should display the default help when helpShow set to true', () => {
      const argv: string[] = ['node', 'index.js', '--help'];
      const scanargs: Scanargs = new Scanargs(argv);
      // to avoid a process.exit which make the tests fail
      scanargs.helpExit = false;
      // to call the actual implementation of the spied function
      spyOn<any>(scanargs, '_displayHelp').and.callThrough();
      scanargs.scan();
      // to test a private method
      expect(scanargs['_displayHelp']).toHaveBeenCalled();
    });

    it('Should not display the default help when helpShow set to false', () => {
      const argv: string[] = ['node', 'index.js', '--help'];
      const scanargs: Scanargs = new Scanargs(argv);
      scanargs.helpExit = false;
      scanargs.showHelp = false;
      spyOn<any>(scanargs, '_displayHelp').and.callThrough();
      scanargs.scan();
      expect(scanargs['_displayHelp']).not.toHaveBeenCalled();
    });
  });

  describe('Method calls', () => {
    it('Should retrieve all option properties when passed to the constructor', () => {
      const option: Option = {
        name: 'port',
        alias: 'p',
        description: 'server port',
        default: 8080,
      };
      const options: Option[] = [option];
      const scanargs: Scanargs = new Scanargs(new Array(), options);

      expect(scanargs.options).toEqual(options);
    });

    it('Should retrieve all option properties when passed to the setter', () => {
      const option: Option = {
        name: 'port',
        alias: 'p',
        description: 'server port',
        default: 8080,
      };
      const options: Option[] = [option];
      const scanargs: Scanargs = new Scanargs(new Array());
      scanargs.options = options;

      expect(scanargs.options).toEqual(options);
    });

    it('A default alias property should be retrieved even when this one is not defined', () => {
      const option: Option = {
        name: 'port',
      };
      const options: Option[] = [option];
      const scanargs: Scanargs = new Scanargs(new Array(), options);

      expect(scanargs.options[0].alias).toBe('p');
    });

    it('Should retrieve execPath', () => {
      const argv: string[] = ['node', 'index.js'];
      const scanargs: Scanargs = new Scanargs(argv);
      expect(scanargs.getExecPath()).toBe('node');
    });

    it('Should retrieve filePath', () => {
      const argv: string[] = ['node', 'index.js'];
      const scanargs: Scanargs = new Scanargs(argv);
      expect(scanargs.getFilePath()).toBe('index.js');
    });

    it('Should retrieve the "port" parameter value when "--port=3000" is passed as command argument', () => {
      const option: Option = {
        name: 'port',
      };
      const options: Option[] = [option];

      const argv: string[] = ['node', 'index.js', '--port=3000'];
      const scanargs: Scanargs = new Scanargs(argv, options);
      expect(scanargs.scan().port).toBe('3000');
    });

    it('Should retrieve the "port" parameter value when "-p=3000" is passed as command argument', () => {
      const option: Option = {
        name: 'port',
      };
      const options: Option[] = [option];

      const argv: string[] = ['node', 'index.js', '-p=3000'];
      const scanargs: Scanargs = new Scanargs(argv, options);
      expect(scanargs.scan().port).toBe('3000');
    });

    it('Should retrieve the "port" alias parameter value when "-P=3000" is passed as command argument', () => {
      const option: Option = {
        name: 'port',
        alias: 'P',
      };
      const options: Option[] = [option];

      const argv: string[] = ['node', 'index.js', '-P=3000'];
      const scanargs: Scanargs = new Scanargs(argv, options);
      expect(scanargs.scan().port).toBe('3000');
    });

    it('Should retrieve the "port" parameter value when argument "--port=3000" is passed and a default value is defined', () => {
      const option: Option = {
        name: 'port',
        default: 8000,
      };
      const options: Option[] = [option];

      const argv: string[] = ['node', 'index.js', '--port=3000'];
      const scanargs: Scanargs = new Scanargs(argv, options);
      expect(scanargs.scan().port).toBe(3000);
    });

    it('Should retrieve the "port" parameter default value when argument "--port=" is passed and a default value is defined', () => {
      const option: Option = {
        name: 'port',
        default: 8000,
      };
      const options: Option[] = [option];

      const argv: string[] = ['node', 'index.js', '--port='];
      const scanargs: Scanargs = new Scanargs(argv, options);
      expect(scanargs.scan().port).toBe(8000);
    });

    it('Should retrieve the "port" parameter default value when no "port" argument is passed', () => {
      const option: Option = {
        name: 'port',
        default: 3000,
      };
      const options: Option[] = [option];

      const argv: string[] = ['node', 'index.js'];
      const scanargs: Scanargs = new Scanargs(argv, options);
      expect(scanargs.scan().port).toBe(3000);
    });

    it('Should retrieve the "port" parameter default value when a "port" argument without value is passed', () => {
      const option: Option = {
        name: 'port',
        default: 3000,
      };
      const options: Option[] = [option];

      const argv: string[] = ['node', 'index.js', '--port'];
      const scanargs: Scanargs = new Scanargs(argv, options);
      expect(scanargs.scan().port).toBe(3000);
    });

    it('Should retrieve the "logger" parameter default boolean value when no logger argument is passed', () => {
      const option: Option = {
        name: 'logger',
        default: true,
      };
      const options: Option[] = [option];

      const argv: string[] = ['node', 'index.js'];
      const scanargs: Scanargs = new Scanargs(argv, options);
      expect(scanargs.scan().logger).toBe(true);
    });

    it('Should retrieve a "port" parameter value set to "" when only argument "--host=localhost" is passed', () => {
      const option: Option = {
        name: 'port',
      };
      const options: Option[] = [option];

      const argv: string[] = ['node', 'index.js', '--host=localhost'];
      const scanargs: Scanargs = new Scanargs(argv, options);
      expect(scanargs.scan().port).toBe('');
    });

    it('Should not retrieve a "port" parameter value when no option are defined', () => {
      const argv: string[] = ['node', 'index.js', '--port=3000'];
      const scanargs: Scanargs = new Scanargs(argv, new Array());
      expect(scanargs.scan().port).not.toBeDefined();
    });

    it('Should retrieve "port" parameter value when arguments "--port 3000" are passed', () => {
      const option: Option = {
        name: 'port',
      };
      const options: Option[] = [option];

      const argv: string[] = ['node', 'index.js', '--port', '3000'];
      const scanargs: Scanargs = new Scanargs(argv, options);
      expect(scanargs.scan().port).toBe('3000');
    });

    it('Should retrieve "port" parameter value when arguments "-p 3000" are passed', () => {
      const option: Option = {
        name: 'port',
      };
      const options: Option[] = [option];

      const argv: string[] = ['node', 'index.js', '-p', '3000'];
      const scanargs: Scanargs = new Scanargs(argv, options);
      expect(scanargs.scan().port).toBe('3000');
    });

    it('Should retrieve true when a default boolean parameter is passed without value', () => {
      const option: Option = {
        name: 'logger',
        default: false,
      };
      const options: Option[] = [option];

      const argv: string[] = ['node', 'index.js', '--logger'];
      const scanargs: Scanargs = new Scanargs(argv, options);
      expect(scanargs.scan().logger).toBe(true);
    });
  });

  describe('Types of arguments values', () => {
    it('A number parameter value without a default value should be of string type', () => {
      const option: Option = {
        name: 'port',
      };
      const options: Option[] = [option];

      const argv: string[] = ['node', 'index.js', '--port=3000'];
      const scanargs: Scanargs = new Scanargs(argv, options);
      expect(typeof scanargs.scan().port).toBe('string');
    });

    it('A parameter value with a number default value should be of number type', () => {
      const option: Option = {
        name: 'port',
        default: 8080,
      };
      const options: Option[] = [option];

      const argv: string[] = ['node', 'index.js', '--port=3000'];
      const scanargs: Scanargs = new Scanargs(argv, options);
      expect(typeof scanargs.scan().port).toBe('number');
    });

    it('A boolean parameter value without a default value should be of string type', () => {
      const option: Option = {
        name: 'verbose',
      };
      const options: Option[] = [option];

      const argv: string[] = ['node', 'index.js', '--verbose=false'];
      const scanargs: Scanargs = new Scanargs(argv, options);
      expect(typeof scanargs.scan().verbose).toBe('string');
    });

    it('A boolean parameter value with a boolean default value should be of boolean type', () => {
      const option: Option = {
        name: 'verbose',
        default: true,
      };
      const options: Option[] = [option];

      const argv: string[] = ['node', 'index.js', '--verbose=false'];
      const scanargs: Scanargs = new Scanargs(argv, options);
      expect(typeof scanargs.scan().verbose).toBe('boolean');
    });
  });

  describe('Getting args or env values according to their settings (priority)', () => {
    beforeEach(() => {
      process.env = { ...savedEnv };
      process.env.port = '1000';
    });

    afterEach(() => {
      process.env = savedEnv;
    });

    it('Should retrieve "1000" as "port" value when [process.env.port="1000"]', () => {
      const option: Option = {
        name: 'port',
      };
      const options: Option[] = [option];

      const argv: string[] = ['node', 'index.js'];
      const scanargs: Scanargs = new Scanargs(argv, options);
      expect(scanargs.scan().port).toBe('1000');
    });

    it('Should retrieve "3000" as "port" value when [--port "3000" && process.env.port="1000"]', () => {
      const option: Option = {
        name: 'port',
      };
      const options: Option[] = [option];

      const argv: string[] = ['node', 'index.js', '--port', '3000'];
      const scanargs: Scanargs = new Scanargs(argv, options);
      expect(scanargs.scan().port).toBe('3000');
    });
  });

  describe('Getting config file values according to its setting or not', () => {
    it('Should retrieve "2000" as "port" value when [.config.json file contents {port: "2000"}]', () => {
      const option: Option = {
        name: 'port',
      };
      const options: Option[] = [option];

      const argv: string[] = ['node', 'index.js'];
      const scanargs: Scanargs = new Scanargs(argv, options);
      scanargs['_jsonConfig'] = { port: '2000' };
      expect(scanargs.scan().port).toBe('2000');
    });
  });
});
