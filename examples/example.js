const { Scanargs } = require('../build/scanargs');
const scan = new Scanargs(process.argv, [{name: 'param'}]);

// command: node example.js --param=azerty
// result: bin [ /usr/bin/node ] file [ /usr/bin/node ] param [ azerty ]
console.log(`bin [ ${scan.getNode()} ] file [ ${scan.getNode()} ] param [ ${scan.scan().param} ]`);