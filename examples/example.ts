import { Option, OptionsValues } from '../sources/option';
import { Scanargs } from '../sources/scanargs';

let scan: Scanargs;
let result: OptionsValues;
let options: Option[];

// Constructing with 1 parameter
scan = new Scanargs(process.argv);
options = [{ name: 'port', alias:'pt', default: 3000, description: 'port number' }, { name: 'logger', default: false }, { name: 'throwError' }];
scan.options = options;
// scan.helpExit = false;
// scan.showHelp = true;
result = scan.scan();

// command: /home/node_modules/.bin/ts-node example.ts --port="8080" --throwError=false --arg=azerty
// result: { "port": 8080, "logger": false, "throwError": "false" }
console.log(`Creation with 2 parameters: ${JSON.stringify(result, null, 2)}`);

// Constructing with 2 parameters
options = [{ name: 'port', default: 3000 }, { name: 'logger', default: true }];
scan = new Scanargs(process.argv, options);
result = scan.scan();
// result: { "port": 8080, "logger": true }
console.log(`Creation with 1 parameter: ${JSON.stringify(result, null, 2)}`);